import React, { PureComponent } from 'react';
import { View, Alert, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { GetLocation_Autocomplate } from './src/Action/index'



class LocationItem extends React.Component {
    _handlePress = async () => {
        const { GetLocation } = this.props
        await this.props.fetchDetails(this.props.place_id)
            .then(data => GetLocation(data) )
            .then(this.props.navigation.goBack())

    }

    render() {

        return (
            <TouchableOpacity style={styles.root} onPress={this._handlePress}>
                <Text>{this.props.description}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        height: 40,
        borderBottomWidth: StyleSheet.hairlineWidth,
        justifyContent: 'center'
    }
})

export default connect(
    state => {
        return {
            listdata: state
        }
    },
    dispatch => {
        return {
            GetLocation: (index) => dispatch(GetLocation_Autocomplate(index))
        }
    })(LocationItem)

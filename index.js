/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { AppRegistry } from "react-native";
//import Router from './src/Router/index'
import App from "./App";

AppRegistry.registerComponent("LocationTracking", () => App);

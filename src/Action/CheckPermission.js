import { Yes_Permission, No_Permission } from './type'
import { PermissionsAndroid } from "react-native";

export const CheckTask = () => {
    return async (dispatch) => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Cool youPermission',
                    message:
                        'Cool',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                dispatch({
                    type: Yes_Permission,
                    data: true
                })
            } else {
                dispatch({
                    type: No_Permission,
                    data: false
                })
            }
        } catch (err) {
            console.warn(err);
        }
    }
}

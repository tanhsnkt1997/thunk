import { GetLocation_Ok, Autocomplate } from './type'

export const CheckLocation = () => {
    return (dispatch) => {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log('Vi tri ========>', position)
                dispatch({
                    type: GetLocation_Ok,
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                })
            },
            (error) => console.log(error),
        );
    }
}


export const GetLocation_Autocomplate = (lalong) => {
    return {
        type: Autocomplate,
        data: lalong
    }
}



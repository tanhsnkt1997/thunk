import { combineReducers, createStore, applyMiddleware } from 'redux'
import AllReducer from '../Reducer/index'
import thunk from 'redux-thunk'

const store = createStore(AllReducer, applyMiddleware(thunk))
export default store
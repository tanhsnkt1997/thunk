import { GetLocation_Ok, Autocomplate } from '../Action/type'


// sate default


let test = {
    data: {
        name: '',
        location: {}
    }
}

const TaskListReducer = (state = test, action) => {
    switch (action.type) {
        case Autocomplate:
            return { ...state, data: { name: action.data.name, location: action.data.geometry.location } }
    }
    return state
}



export default TaskListReducer
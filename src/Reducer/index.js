import { combineReducers } from 'redux'
import PossitionReducer from './PossitionReducer'
import Check from './CheckPermissionReducer'
import Location_Autocomplate from './Location_AutoComplate'

const allReducer = combineReducers({
    test: PossitionReducer,
    Permission: Check,
    Location_Autocomplate: Location_Autocomplate
})
export default allReducer
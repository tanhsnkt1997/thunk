import { createStackNavigator, createAppContainer, createBottomTabNavigator, createSwitchNavigator } from "react-navigation";
import Checklocation from '../Main/Checklocation/index'
import MainComponent from '../Main/main'


const switchchat = createStackNavigator({
    MainComponent: MainComponent,
    Checklocation: Checklocation
});

export default createAppContainer(switchchat);
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  PermissionsAndroid, TextInput, ScrollView, Image, Dimensions
} from "react-native";
import { GoogleAutoComplete, GoogleLocationResult } from 'react-native-google-autocomplete';
import LocationItem from '../../../LocationItem'

const { height, width } = Dimensions.get('window')

class AnimatedMarkers extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <GoogleAutoComplete apiKey="AIzaSyDjn0Uytv_FSUwwpOUTVCvL4vKYU7Ev7VU" debounce={500} minLength={3}>
            {({ inputValue, handleTextChange, locationResults, fetchDetails }) => (
              <React.Fragment>
                <TextInput
                  style={{
                    height: 50,
                    width: width - 15,
                    borderWidth: 1,
                    paddingHorizontal: 16,
                  }}
                  value={inputValue}
                  onChangeText={handleTextChange}
                  placeholder="Location..."
                />
                <ScrollView style={{ height: 600 }}>
                  {locationResults.map((el, i) => (
                    <LocationItem
                      {...this.props}
                      {...el}
                      key={el.id}
                      fetchDetails={fetchDetails}
                    />
                  ))}
                </ScrollView>
              </React.Fragment>
            )}
          </GoogleAutoComplete>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
  },

  container1: {
    height: '100%',
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: 20,
    backgroundColor: "transparent"
  },
  ic_gps_Container: {
    flexDirection: "row",
    marginVertical: 20,
    backgroundColor: "transparent"
  },

  icongps: {
    height: 40,
    width: 40
  }
});

export default AnimatedMarkers;


/*
  <View>
          <GoogleAutoComplete apiKey="AIzaSyDjn0Uytv_FSUwwpOUTVCvL4vKYU7Ev7VU" debounce={500} minLength={3}>
            {({ inputValue, handleTextChange, locationResults, fetchDetails }) => (
              <React.Fragment>
                <TextInput
                  style={{
                    height: 40,
                    width: 300,
                    borderWidth: 1,
                    paddingHorizontal: 16,
                  }}
                  value={inputValue}
                  onChangeText={handleTextChange}
                  placeholder="Location..."
                />
                <ScrollView style={{ maxHeight: 100 }}>
                  {locationResults.map((el, i) => (
                    <LocationItem
                      {...el}
                      key={el.id}
                      fetchDetails={fetchDetails}
                    />
                  ))}
                </ScrollView>
              </React.Fragment>
            )}
          </GoogleAutoComplete>
        </View>

    /////////////////////////

    <View>
          <MapView
            style={styles.map}
            provider={PROVIDER_GOOGLE}
            showUserLocation
            followUserLocation
            loadingEnabled
            region={this.getMapRegion()}
          >
            <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
            <Marker.Animated
              ref={marker => {
                this.marker = marker;
              }}
              coordinate={this.state.coordinate}
            />
          </MapView>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={[styles.bubble, styles.button]}>
            <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
            </Text>
          </TouchableOpacity>
        </View>
*/



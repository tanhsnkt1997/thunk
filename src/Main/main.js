
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  PermissionsAndroid, TextInput, ScrollView, Image, Dimensions
} from "react-native";
import MapView, {
  Marker,
  AnimatedRegion,
  Polyline,
  PROVIDER_GOOGLE
} from "react-native-maps";
import haversine from "haversine"
import { GoogleAutoComplete, GoogleLocationResult } from 'react-native-google-autocomplete';
import iconGps from '../icon/logo_gpss.png'
import MapViewDirections from 'react-native-maps-directions';
import MapContainer from '../Components/MapContainer/index'
import { connect } from 'react-redux'
import { CheckTask, CheckLocation } from '../Action/index'
import { InputGroup, Input } from "native-base";
var width = Dimensions.get("window").width; //full width


const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = 0;
const LONGITUDE = 0;


class MapMarker extends Component {

  constructor(props) {
    super(props);

    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      routeCoordinates: [],
      distanceTravelled: 0,
      prevLatLng: {},
      coordinate: new AnimatedRegion({
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: 0,
        longitudeDelta: 0
      }),
    };

  }


  componentWillMount() {
    this.props.CheckTask()
  }


  componentDidMount() {
    this.props.CheckLocation()
  }

  WatchPossition = () => {
    const { coordinate } = this.state;
    this.watchID = navigator.geolocation.watchPosition(
      position => {
        const { routeCoordinates, distanceTravelled } = this.state;
        const { latitude, longitude } = position.coords;

        const newCoordinate = {
          latitude,
          longitude
        };
        console.log({ newCoordinate });

        if (Platform.OS === "android") {
          if (this.marker) {
            this.marker._component.animateMarkerToCoordinate(
              newCoordinate,
              500
            );
          }
        } else {
          coordinate.timing(newCoordinate).start();
        }

        this.setState({
          latitude,
          longitude,
          routeCoordinates: routeCoordinates.concat([newCoordinate]),
          distanceTravelled:
            distanceTravelled + this.calcDistance(newCoordinate),
          prevLatLng: newCoordinate
        });
      },
      error => console.log(error),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000, distanceFilter: 10 },
    );
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  getMapRegion = ({ latitude, longitude } = this.props.listData.test.data) => ({

    latitude: latitude,
    longitude: longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA
  });

  calcDistance = newLatLng => {
    const { prevLatLng } = this.state;
    return haversine(prevLatLng, newLatLng) || 0;
  };


  render() {
    console.log(this.props)
    const { name } = this.props.listData.Location_Autocomplate.data

    return (
      <View style={styles.container}>
        <MapContainer region={this.getMapRegion()} />

        <View style={styles.searchBox}>
          <View style={styles.inputWrapper}>
            <Text style={styles.label}>PICK UP</Text>
            <InputGroup>

              <Input
                onFocus={() => this.props.navigation.navigate('Checklocation')}
                style={styles.inputSearch}
                placeholder="Choose pick-up location"
                onChangeText={a => console.log(a)}
                value={name}

              />
            </InputGroup>
          </View>
          <View style={styles.secondInputWrapper}>
            <Text style={styles.label}>DROP-OFF</Text>
            <InputGroup>

              <Input

                style={styles.inputSearch}
                placeholder="Choose drop-off location"
                onChangeText={(b) => console.log(b)}

              />
            </InputGroup>
          </View>
        </View>

        <View style={styles.ic_gps_Container}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Checklocation')}>
            <Image style={styles.icongps} source={iconGps} />
          </TouchableOpacity>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={{ backgroundColor: 'red', width: 100, height: 100, borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ textAlign: 'center' }}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,

    flex: 1
  },

  container1: {
    height: '100%',
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 50
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flex: 1,
    marginVertical: 20,
    backgroundColor: "transparent",
    alignItems: 'flex-end',
    justifyContent: 'flex-start',

  },
  ic_gps_Container: {
    flex: 1,
    //  marginVertical: 20,
    backgroundColor: "transparent",
    justifyContent: 'flex-start',
    height: 40,
    width: 40,
  

  },

  icongps: {
    height: 40,
    width: 40,

  },
  searchBox: {
    flex: 1,
    top: 0,
    //position: "absolute",
    width: width
  },
  inputWrapper: {
    marginLeft: 15,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 0,
    backgroundColor: "#fff",
    opacity: 0.9,
    borderRadius: 7
  },
  secondInputWrapper: {
    marginLeft: 15,
    marginRight: 10,
    marginTop: 0,
    backgroundColor: "#fff",
    opacity: 0.9,
    borderRadius: 7
  },
  inputSearch: {
    fontSize: 14
  },
  label: {
    fontSize: 10,
    fontStyle: "italic",
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 0
  }
});


export default connect(
  state => {
    return {
      //  listData: state.data
      listData: state
    }
  }, { CheckTask, CheckLocation })(MapMarker)


/*
  <View>
          <GoogleAutoComplete apiKey="AIzaSyDjn0Uytv_FSUwwpOUTVCvL4vKYU7Ev7VU" debounce={500} minLength={3}>
            {({ inputValue, handleTextChange, locationResults, fetchDetails }) => (
              <React.Fragment>
                <TextInput
                  style={{
                    height: 40,
                    width: 300,
                    borderWidth: 1,
                    paddingHorizontal: 16,
                  }}
                  value={inputValue}
                  onChangeText={handleTextChange}
                  placeholder="Location..."
                />
                <ScrollView style={{ maxHeight: 100 }}>
                  {locationResults.map((el, i) => (
                    <LocationItem
                      {...el}
                      key={el.id}
                      fetchDetails={fetchDetails}
                    />
                  ))}
                </ScrollView>
              </React.Fragment>
            )}
          </GoogleAutoComplete>
        </View>

    /////////////////////////

    <View>
          <MapView
            style={styles.map}
            provider={PROVIDER_GOOGLE}
            showUserLocation
            followUserLocation
            loadingEnabled
            region={this.getMapRegion()}
          >
            <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
            <Marker.Animated
              ref={marker => {
                this.marker = marker;
              }}
              coordinate={this.state.coordinate}
            />
          </MapView>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={[styles.bubble, styles.button]}>
            <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
            </Text>
          </TouchableOpacity>
        </View>
*/


/*
 <View style={styles.container}>

        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          showUserLocation
          followUserLocation
          loadingEnabled
          region={this.getMapRegion()}
        >
          <MapViewDirections
            origin={{ latitude: 10.764547216732758, longitude: 106.69408522972192 }}
            destination={{ latitude: 10.754545712541297, longitude: 106.66421704638351 }}
            apikey={GOOGLE_MAPS_APIKEY}
            strokeWidth={3}
            strokeColor='hotpink'
          />

          <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
          <Marker coordinate={this.getMapRegion()} />
          <Marker coordinate={{ latitude: 10.754545712541297, longitude: 106.66421704638351 }} />
          <Marker.Animated
            ref={marker => {
              this.marker = marker;
            }}
            coordinate={this.state.coordinate}
          />
        </MapView>

        <View style={styles.ic_gps_Container}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Checklocation')}>
            <Image style={styles.icongps} source={iconGps} />
          </TouchableOpacity>
        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity style={[styles.bubble, styles.button]}>
            <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
            </Text>
          </TouchableOpacity>
        </View>
      </View>
*/


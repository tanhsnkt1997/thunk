import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Platform,
    PermissionsAndroid, TextInput, ScrollView, Image,
} from "react-native";
import MapView, {
    Marker,
    AnimatedRegion,
    Polyline,
    PROVIDER_GOOGLE
} from "react-native-maps";
import styles from './style'
import MapViewDirections from 'react-native-maps-directions';


const Marker = [{ latitude: 10.754545712541297, longitude: 106.66421704638351 },{ latitude: 10.771111, longitude: 106.6975 },  ]

export const MapContainer = ({ region }) => {
    const GOOGLE_MAPS_APIKEY = 'AIzaSyDjn0Uytv_FSUwwpOUTVCvL4vKYU7Ev7VU';
    return (
        <MapView
            provider={MapView.PROVIDER_GOOGLE}
            showUserLocation
            followUserLocation
            loadingEnabled
            style={styles.map}
            region={region}
        >
            <MapViewDirections
                origin={{ latitude: 10.764547216732758, longitude: 106.69408522972192 }}
                destination={{ latitude: 10.754545712541297, longitude: 106.66421704638351 }}
                apikey={GOOGLE_MAPS_APIKEY}
                strokeWidth={3}
                strokeColor='hotpink'
            />

            <Marker coordinate={region} />
            <Marker coordinate={Marker.map((data) =>data)} />
        </MapView>
    )
}

export default MapContainer
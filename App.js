
import React from "react";
import RootRouter from './src/Router/index'
import { Provider } from 'react-redux'
//import store from './src/Store/index'
import GetCurent from './src/Reducer/PossitionReducer'
import thunk from 'redux-thunk'
import { combineReducers, createStore, applyMiddleware } from 'redux'
import AllReducer from './src/Reducer/index'
import MainComponent from './src/Main/main'
import store from './src/Store/index'



export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <RootRouter />
      </Provider>
    );
  }
}




/*
  <View>
          <GoogleAutoComplete apiKey="AIzaSyDjn0Uytv_FSUwwpOUTVCvL4vKYU7Ev7VU" debounce={500} minLength={3}>
            {({ inputValue, handleTextChange, locationResults, fetchDetails }) => (
              <React.Fragment>
                <TextInput
                  style={{
                    height: 40,
                    width: 300,
                    borderWidth: 1,
                    paddingHorizontal: 16,
                  }}
                  value={inputValue}
                  onChangeText={handleTextChange}
                  placeholder="Location..."
                />
                <ScrollView style={{ maxHeight: 100 }}>
                  {locationResults.map((el, i) => (
                    <LocationItem
                      {...el}
                      key={el.id}
                      fetchDetails={fetchDetails}
                    />
                  ))}
                </ScrollView>
              </React.Fragment>
            )}
          </GoogleAutoComplete>
        </View>

    /////////////////////////

    <View>
          <MapView
            style={styles.map}
            provider={PROVIDER_GOOGLE}
            showUserLocation
            followUserLocation
            loadingEnabled
            region={this.getMapRegion()}
          >
            <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
            <Marker.Animated
              ref={marker => {
                this.marker = marker;
              }}
              coordinate={this.state.coordinate}
            />
          </MapView>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={[styles.bubble, styles.button]}>
            <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
            </Text>
          </TouchableOpacity>
        </View>
*/


/*
 <View style={styles.container}>

        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          showUserLocation
          followUserLocation
          loadingEnabled
          region={this.getMapRegion()}
        >
          <MapViewDirections
            origin={{ latitude: 10.764547216732758, longitude: 106.69408522972192 }}
            destination={{ latitude: 10.754545712541297, longitude: 106.66421704638351 }}
            apikey={GOOGLE_MAPS_APIKEY}
            strokeWidth={3}
            strokeColor='hotpink'
          />

          <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
          <Marker coordinate={this.getMapRegion()} />
          <Marker coordinate={{ latitude: 10.754545712541297, longitude: 106.66421704638351 }} />
          <Marker.Animated
            ref={marker => {
              this.marker = marker;
            }}
            coordinate={this.state.coordinate}
          />
        </MapView>

        <View style={styles.ic_gps_Container}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Checklocation')}>
            <Image style={styles.icongps} source={iconGps} />
          </TouchableOpacity>
        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity style={[styles.bubble, styles.button]}>
            <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
            </Text>
          </TouchableOpacity>
        </View>
      </View>
*/

